package Modules;

public class Fish extends Aquatic {
    private String color;
    private boolean isPredator;
    private String tailType;

    public Fish(long id, String name, double weight, int age, String gender, String color, boolean isPredator, String tailType) {
        super(id, name, weight, age, gender);
        this.color = color;
        this.isPredator = isPredator;
        this.tailType = tailType;
    }

    public Fish() { }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isPredator() {
        return isPredator;
    }

    public void setPredator(boolean predator) {
        isPredator = predator;
    }

    public String getTailType() {
        return tailType;
    }

    public void setTailType(String tailType) {
        this.tailType = tailType;
    }
}
