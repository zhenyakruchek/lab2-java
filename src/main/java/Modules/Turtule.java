package Modules;

public class Turtule extends Aquatic {
    private boolean isSkinBreather;
    private double shellSize;
    private boolean flippers;

    public Turtule(long id, String name, double weight, int age, String gender, boolean isSkinBreather, double shellSize, boolean flippers) {
        super(id, name, weight, age, gender);
        this.isSkinBreather = isSkinBreather;
        this.shellSize = shellSize;
        this.flippers = flippers;
    }

    public Turtule() { }

    public boolean isSkinBreather() {
        return isSkinBreather;
    }

    public void setSkinBreather(boolean skinBreather) {
        isSkinBreather = skinBreather;
    }

    public double getShellSize() {
        return shellSize;
    }

    public void setShellSize(double shellSize) {
        this.shellSize = shellSize;
    }

    public boolean isFlippers() {
        return flippers;
    }

    public void setFlippers(boolean flippers) {
        this.flippers = flippers;
    }
}
