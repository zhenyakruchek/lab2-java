package Modules;

public class User {
    private long id;
    private String username;
    private short age;
    private Aquarium aquarium;

    public User(long id, String username, short age, Aquarium aquarium) {
        this.id = id;
        this.username = username;
        this.age = age;
        this.aquarium = aquarium;
    }

    public User(String username, short age, Aquarium aquarium) {
        this.username = username;
        this.age = age;
        this.aquarium = aquarium;
    }

    public User() { }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public short getAge() {
        return age;
    }

    public void setAge(short age) {
        this.age = age;
    }

    public Aquarium getAquarium() {
        return aquarium;
    }

    public void setAquarium(Aquarium aquarium) {
        this.aquarium = aquarium;
    }
}
