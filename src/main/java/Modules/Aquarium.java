package Modules;

import java.util.List;

public class Aquarium {
    private int id;
    private double width;
    private double height;
    private double length;
    private List<Turtule> turtleList;
    private List<Fish> fishList;

    public Aquarium(int id, double width, double height, double length, List<Turtule> turtleList, List<Fish> fishList) {
        this.id = id;
        this.width = width;
        this.height = height;
        this.length = length;
        this.turtleList = turtleList;
        this.fishList = fishList;
    }

    public Aquarium(int id, double width, double height, double length) {
        this.id = id;
        this.width = width;
        this.height = height;
        this.length = length;
    }

    public Aquarium(double width, double height, double length) {
        this.width = width;
        this.height = height;
        this.length = length;
    }

    public Aquarium() { }

    public List<Turtule> getTurtleList() { return turtleList; }

    public void setTurtleList(List<Turtule> turtleList) { this.turtleList = turtleList; }

    public List<Fish> getFishList() { return fishList; }

    public void setFishList(List<Fish> fishList) { this.fishList = fishList; }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }
}
