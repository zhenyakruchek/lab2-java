package DAO;

import Modules.Aquarium;
import Modules.Fish;
import Modules.Turtule;
import Modules.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDAOImpl extends DAOImpl<Integer, User> {

    public static final String SQL_SELECT_ALL_USERS = "select * from users as u" +
                                                      " join aquariums as aq" +
                                                      " on u.aquarium_id = aq.id";
    public static final String SQL_SELECT_ALL_TURTULES = "select * from turtules" +
                                                         " where aquarium_id = ?";
    public static final String SQL_SELECT_ALL_FISHES = "select * from fishes" +
                                                       " where aquarium_id = ?";
    public static final String SQL_SELECT_USER_BY_ID = "select * from users as u" +
                                                       " join aquariums as aq" +
                                                       " on u.aquarium_id = aq.id" +
                                                       " and u.id = ?";


    public UserDAOImpl(Connection connection) {
        super(connection);
    }

    @Override
    public User getByID(Integer id) throws SQLException {
        User user = new User();
        PreparedStatement st = connection.prepareStatement(SQL_SELECT_USER_BY_ID);
        st.setInt(1, id);
        final ResultSet resultSet = st.executeQuery();
        if (resultSet.next()) {
            user.setId(id);
            user.setUsername(resultSet.getString("username"));
            user.setAge(resultSet.getShort("age"));

            Aquarium aquarium = new Aquarium();
            aquarium.setId(resultSet.getInt(5));
            aquarium.setWidth(resultSet.getDouble("width"));
            aquarium.setLength(resultSet.getDouble("lenght"));
            aquarium.setHeight(resultSet.getDouble("height"));
            aquarium.setTurtleList(getTurtlesToAq(aquarium.getId()));
            aquarium.setFishList(getFishesToAq(aquarium.getId()));

            user.setAquarium(aquarium);
        }
        return user;
    }

    @Override
    public List<User> getAll() throws SQLException {
        List<User> users = new ArrayList<>();
        Statement st = connection.createStatement();
        ResultSet resultSet = st.executeQuery(SQL_SELECT_ALL_USERS);

        while (resultSet.next()) {
            User user = new User();
            user.setId(resultSet.getLong("id"));
            user.setUsername(resultSet.getString("username"));
            user.setAge(resultSet.getShort("age"));

            Aquarium aquarium = new Aquarium();
            aquarium.setId(resultSet.getInt(5));
            aquarium.setWidth(resultSet.getDouble("width"));
            aquarium.setLength(resultSet.getDouble("lenght"));
            aquarium.setHeight(resultSet.getDouble("height"));
            aquarium.setTurtleList(getTurtlesToAq(aquarium.getId()));
            aquarium.setFishList(getFishesToAq(aquarium.getId()));

            user.setAquarium(aquarium);

            users.add(user);
        }
        return users;
    }

    private List<Turtule> getTurtlesToAq(int id) throws SQLException {
        List<Turtule> turtles = new ArrayList<>();
        PreparedStatement st = connection.prepareStatement(SQL_SELECT_ALL_TURTULES);
        st.setInt(1, id);
        final ResultSet resultSet = st.executeQuery();
        while(resultSet.next()){
            Turtule t = new Turtule();
            t.setId(resultSet.getLong("id"));
            t.setName(resultSet.getString("name"));
            t.setWeight(resultSet.getDouble("weight"));
            t.setAge(resultSet.getInt("age"));
            t.setGender(resultSet.getString("gender"));
            t.setSkinBreather(resultSet.getBoolean("isSkinBreather"));
            t.setShellSize(resultSet.getDouble("shellSize"));
            t.setFlippers(resultSet.getBoolean("flippers"));
            turtles.add(t);
        }
        return turtles;
    }


    private List<Fish> getFishesToAq(int id) throws SQLException {
        List<Fish> fishes = new ArrayList<>();
        PreparedStatement st = connection.prepareStatement(SQL_SELECT_ALL_FISHES);
        st.setInt(1, id);
        final ResultSet resultSet = st.executeQuery();
        while(resultSet.next()){
            Fish f = new Fish();
            f.setId(resultSet.getLong("id"));
            f.setName(resultSet.getString("name"));
            f.setWeight(resultSet.getDouble("weight"));
            f.setAge(resultSet.getInt("age"));
            f.setGender(resultSet.getString("gender"));
            f.setColor(resultSet.getString("color"));
            f.setPredator(resultSet.getBoolean("isPredator"));
            f.setTailType(resultSet.getString("tailType"));
            fishes.add(f);
        }
        return fishes;
    }
}

