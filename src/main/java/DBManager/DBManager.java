package DBManager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBManager {
    final static private String user = "postgres";
    final static private String password = "jeksonthebest";
    final static private String url = "jdbc:postgresql://localhost:5432/postgres?currentSchema=aquarium";

    public static Connection connectToDB() throws SQLException {

        return DriverManager.getConnection(url, user, password);
    }

    public static void disconnectFromDB(Connection connection) throws SQLException {
        connection.close();
    }
}
