package DAO;

import DBManager.DBManager;
import Modules.Aquarium;
import Modules.Fish;
import Modules.Turtule;
import Modules.User;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class UserDAOImplTest {

    DBManager dbManager = new DBManager();
    Connection con = dbManager.connectToDB();
    UserDAOImpl uDAO = new UserDAOImpl(con);

    public UserDAOImplTest() throws SQLException {
    }

    @Test
    public void getByID() throws SQLException {
        Aquarium aquarium = new Aquarium(1.4, 1.5, 1.6);
        List<Fish> fishList = new ArrayList<Fish>();
        fishList.add(new Fish(1,"Masha", 45.6, 19, "female", "yellow", true, "pointed"));
        fishList.add(new Fish(5,"test", 4, 5, "female", "blue", true, "square"));
        aquarium.setFishList(fishList);
        List<Turtule> turtules = new ArrayList<Turtule>();
        turtules.add(new Turtule(1,"Darina", 60, 19, "female", true, 5.7, false));
        turtules.add(new Turtule(4,"Max", 45, 22, "male", false, 14.88, true));
        aquarium.setTurtleList(turtules);
        User user = new User(1, "solosuicide", (short) 19, aquarium);

        User user1 = uDAO.getByID(1);
        assertEquals(user, user1);
    }

    @Test
    public void getAll() throws SQLException {
        List<User> users = uDAO.getAll();
        for (User user : users) {
            System.out.println(user.getUsername());
        }
    }
}